#include <sys/types.h>	//fifo
#include <sys/stat.h>	//fifo
#include <stdio.h>	//perror
#include <stdlib.h>	//exit
#include <fcntl.h>	//open
#include <unistd.h>	//read

#define N 56

int main()
{
	int e_t; //error test	
	int fd; //descriptor de la fifo
	int fifo_error; //determina si hay error por lectura o cierre en la fifo	
	const char* fifo = "/tmp/fifo";	//ruta de la fifo
	char buffer [N];		//buffer es un puntero a char


	if(mkfifo(fifo, 0777)!=0){		//creamos la fifo con permisos 
		perror("mkfifo");		//comprobando si se ha creado 
		exit(1);			//bien
	}

	fd = open(fifo, O_RDONLY);		//abrimos la fifo y comprobamos

	if(fd==-1){
		perror("open fifo");
		exit(1);
	}
	while(1){				//leemos de la fifo y escribimos

		fifo_error = read(fd,&buffer,N*sizeof(char));
		
		if(fifo_error==-1){			
			perror("error read from fifo(logger), se cierra logger");
			exit(1);
		}
		else if(fifo_error==0){			
			perror("cerrada la fifo from client(logger), se cierra logger");
			exit(1);
		}
		fifo_error = write(1,buffer,N*sizeof(char)); 		
		if(fifo_error == -1){
			perror("error write from fifo(logger), se cierra logger");
			exit(1);
		}
		else if(fifo_error==0){			
			perror("cerrada la fifo from client(logger), se cierra logger");
			exit(1);
		}
	}
	if(close(fd)!=0 || unlink(fifo)!=0){	//cerramos descriptor y fifo y
		perror("close fifo (logger)");		//comprobamos error
		exit(1);
	}
	return 0;
}
