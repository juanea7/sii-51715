// Mundo.cpp: implementation of the CMundo class.
// JUAN ENCINAS ANCHUSTEGUI 51715
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <unistd.h>	//close
#include <fcntl.h>	//open

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>

#include <signal.h>	//señales

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////



CMundo::CMundo()
{
	fichero_compartido= "/tmp/fichero_compartido";
	Init();
}

CMundo::~CMundo()
{
	pdmc->accion = 5; //se envia un 5 que hara al bot cerrarse
	if(unlink(fichero_compartido) !=0){
		perror("error closing fichero compartido(server), se cierra server");
		exit(1);
	}
	sc_conexion.Close();//se cierra el socket de conexion
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);	//jugador 1 color rojo
	print(cad,10,0,1,0,0);
	sprintf(cad,"Jugador2: %d",puntos2);	//jugador 2 color azul
	print(cad,650,0,0,0,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();

	for(int i=0;i<esferas.size();i++)
	{
		esferas[i]->Dibuja();
	}

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	
		
	if(tiempo--<0)		//cada 30 segundos se añade una esfera nueva
	{
		tiempo=1200;
		esferas.push_back(new Esfera);	
	}
	

	for(int i=0;i<esferas.size();i++)		//modificacion del codigo para utilizar el vector de esferas
	{
		esferas[i]->Mueve(0.025f);
		jugador1.Rebota(*esferas[i]);
		jugador2.Rebota(*esferas[i]);
		if(fondo_izq.Rebota(*esferas[i]))
		{	
			for(int j=0;j<esferas.size();j++)	//elimino todas las esferas
				delete esferas[j];
			esferas.clear();
			Esfera * e= new Esfera;
			e->centro.x=0;

			e->radio=0.5f;	//devuelvo el radio a su valor inicial

			e->centro.y=rand()/(float)RAND_MAX;
			e->velocidad.x=2+2*rand()/(float)RAND_MAX;
			e->velocidad.y=2+2*rand()/(float)RAND_MAX;
			esferas.push_back(e);
			puntos2++;
			
			tiempo=1200;	//vuelve a empezar el temporizador de adicion de esfera
		}

		if(fondo_dcho.Rebota(*esferas[i]))
		{
			for(int j=0;j<esferas.size();j++)
				delete esferas[j];
			esferas.clear();
			Esfera * e= new Esfera;
			e->centro.x=0;

			e->radio=0.5f;	//devuelvo el radio a su valor inicial

			e->centro.y=rand()/(float)RAND_MAX;
			e->velocidad.x=-2-2*rand()/(float)RAND_MAX;
			e->velocidad.y=-2-2*rand()/(float)RAND_MAX;
			esferas.push_back(e);
			puntos1++;
			
			tiempo=1200;	//vuelve a empezar el temporizador de adicion de esfera
		}
	}

	int i;
	for(i=0;i<paredes.size();i++)
	{
		for(int j=0;j<esferas.size();j++)
		{
			paredes[i].Rebota(*esferas[j]);
		}
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}
	


	//datos compartidos

	
	float aux1 = esferas[0]->centro.x;		//se calcula el indice de la esfera mas cercana al pala 1 y 2, que seran las que es intentaran parar
	int i_e1=0;
	for(int i = 0; i < esferas.size(); i++){
		if(esferas[i]->centro.x<aux1)	//para la pala 1 la mas a la izda
			i_e1=i;
	}
	float aux2 = esferas[0]->centro.x;
	int i_e2=0;
	for(int i = 0; i < esferas.size(); i++){
		if(esferas[i]->centro.x>aux2)	//para la pala 2 la mas a la dcha
			i_e2=i;
	}
	
	pdmc->esfera1.centro.x=esferas[i_e1]->centro.x;
	pdmc->esfera1.centro.y=esferas[i_e1]->centro.y;
	pdmc->esfera2.centro.x=esferas[i_e2]->centro.x;
	pdmc->esfera2.centro.y=esferas[i_e2]->centro.y;
	pdmc->raqueta1.y1=jugador1.y1;
	pdmc->raqueta1.y2=jugador1.y2;
	pdmc->raqueta2.y1=jugador2.y1;
	pdmc->raqueta2.y2=jugador2.y2;
	
	if(pdmc->control_r1){		//si el bot esta controlando la pala 1
		if(pdmc->accion==1){
			keystates['w']=true;
			keystates['s']=false;
		}
		else if(pdmc->accion==-1){
			keystates['w']=false;
			keystates['s']=true;
		}
		else if(pdmc->accion==0){
			keystates['w']=false;
			keystates['s']=false;
		}
	}
	if(pdmc->control_r2){		//si el bot esta controlando la pala 2
		if(pdmc->accion2==1){
			keystates['o']=true;
			keystates['l']=false;
		}
		else if(pdmc->accion2==-1){
			keystates['o']=false;
			keystates['l']=true;
		}
		else if(pdmc->accion==0){
			keystates['o']=false;
			keystates['l']=false;
		}
	}
	
	//socket server to cliente
	char cad_ffms[200];
	sprintf(cad_ffms,"%f %f %f %f %f %f %f %f %f %f %d %d", esferas[0]->centro.x,esferas[0]->centro.y, jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2, puntos1, puntos2);
	
	if(sc_comunicacion.Send(cad_ffms,sizeof(cad_ffms))<0)
	{
		perror("Error al enviar coordenadas al cliente (servidor)");
		this->~CMundo();
	}

	
}

void CMundo::Keyboard() 	//control del teclado
{
	
	if (keystates['w']) 
		jugador1.velocidad.y=5.0f;
	else if (keystates['s'])
		jugador1.velocidad.y=-5.0f;
	else 
		jugador1.velocidad.y=0;

	if (keystates['o']) 
		jugador2.velocidad.y=5.0f;
	else if (keystates['l'])
		jugador2.velocidad.y=-5.0f;
	else 
		jugador2.velocidad.y=0;
	
}


void* hilo_comandos(void* d)
{
      CMundo* p=(CMundo*) d;
      p->RecibeComandosJugador();
}


void CMundo::RecibeComandosJugador()
{
     int ffmct_error;
     while (1) {
            usleep(10);
	    if(sc_comunicacion.Receive((char *)keystates,sizeof(keystates))<0){	    
		perror("cerrada fifo from cliente(server thread),se cierra el server thread");
		this->~CMundo();
	    }
      }
}

void tratar_alarma(int n){		//tratamiento de señales
	if (n == SIGUSR1){
		perror("servidor terminado correctamente(SIGUSR1)");
		exit(n);
	}	
	else{
		perror("servidor terminado por la senyal");
		exit(n);
	}
}

void CMundo::Init()
{

	//socket cliente servidor
	char ip[] = "127.0.0.1";
	int port = 4200;
	char cad_cliente[100];
	printf("esperando conexion\n");
	if(sc_conexion.InitServer(ip,port)<0){
		perror("Error al inicial el socket(servidor)");
		this->~CMundo();
	}
	while(1){
		sc_comunicacion = sc_conexion.Accept();
		break;
	}
	if(sc_comunicacion.Receive(cad_cliente,strlen(cad_cliente)+1)<0){
		perror("Error al leer el identificador del cliente (servidor)");
		this->~CMundo();
	}
	printf("El cliente conectado es %s",cad_cliente);
	
	



	caddr_t dst; //direccion destino

	tiempo=1200;	//inicio tiempo con 1200 que entre 25 ms da 30 segundos	
	esferas.push_back(new Esfera);	//creo esfera y la añado al vector

	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;
	fondo_izq.setColor(0,1,0);		//la porteria izquierda sera verde

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;
	fondo_dcho.setColor(0,1,0);		//la porteria derecha sera verde

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;
	jugador1.setColor(1,0,0);		//jugador 1 tendra color rojo

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
	jugador2.setColor(0,0,1);		//jugador 2 tendra color rojo
	
	
	
	
	//datos compartidos
	
	
	if((fdc=open(fichero_compartido, O_CREAT|O_TRUNC|O_RDWR,0777))<0){
		perror("No puede crearse el fichero compartido(server), se cierra el server");
		close(fdc);
		this->~CMundo();		
		//exit(1);
	}
	if(ftruncate(fdc,sizeof(dmc))<0){
		perror("error en ftruncate(server), se cierra el server");
		close(fdc);
		this->~CMundo();		
		//exit(1);
	}
	if((dst=(caddr_t)mmap((caddr_t)0, sizeof(dmc), PROT_WRITE | PROT_READ, MAP_SHARED, fdc,0))== MAP_FAILED){
		perror("Error en la proyeccion del fichero compartido(server), se cierra el server");
		close(fdc);
		this->~CMundo();		
		//exit(1);
	}
	//cierro fichero
	close(fdc);
	pdmc=(DatosMemCompartida*)dst;

	
	pthread_create(&thid1, NULL, hilo_comandos, this);
	
	//tratamiento de señales
	
	struct sigaction act;
	sigset_t mask;
	/* estable el manejador */
	act.sa_handler = tratar_alarma;
	/* función a ejecutar */
	act.sa_flags = SA_RESTART;
	/* ninguna acción específica*/
	sigemptyset(&act.sa_mask);
	sigaction(SIGINT, &act,NULL);
	sigaction(SIGTERM, &act,NULL);
	sigaction(SIGPIPE, &act,NULL);
	sigaction(SIGUSR1, &act,NULL);
	

}


