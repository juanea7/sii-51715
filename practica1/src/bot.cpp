#include "DatosMemCompartida.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>


int main(){

	DatosMemCompartida* pdmc;
	const char* fichero_compartido = "/tmp/fichero_compartido";
	int fdc;
	caddr_t dst; //puntero inicio fichero compartido
	
	struct stat bstat;

	int contador=400;	//lleva la cuenta de ciclos en los que no se mueve la raqueta 2 (de forma que a loa 10 segundos tome el control el bot|400ciclos=10000ms=10segundos|)
	float aux;	//se usa para comprobar si se mueve la pala 2
	
	
	if((fdc=open(fichero_compartido,O_RDWR,0777))==-1){
		perror("No puede abrir el fichero compartido(bot), se cierra el bot");
		close(fdc);
		exit(1);
	}
	
	if(fstat(fdc,&bstat)<0){
		perror("Error en fstat del fichero, se cierra el bot");
		close(fdc);
		exit(1);
	}	

	if((dst=(caddr_t)mmap((caddr_t)0, bstat.st_size, PROT_WRITE | PROT_READ, MAP_SHARED, fdc,0))== MAP_FAILED){
		perror("Error en la proyeccion del fichero compartido(bot), se cierra el bot");
		close(fdc);
		exit(1);
	}
	pdmc =(DatosMemCompartida*) dst;
	close(fdc);
	unlink(fichero_compartido);
	pdmc->control_r1=true;		//bot controla la pala 1 de inicio
	pdmc->control_r2=false;		//bot no controla la pala 2
	aux=pdmc->raqueta2.y1;		//almacenara la posicion de la pala en el instante t-1
	while(1){
		if (pdmc->accion == 5){	//comprueba si el servidor se ha cerrado, y se cierra en caso afirmativo
			perror("servidor cerrado(bot), se cierra el bot");
			exit(1);
		}
		if(pdmc->esfera1.centro.y>((pdmc->raqueta1.y2+pdmc->raqueta1.y1)/2))	//distancia en eje y entre el punto medio de 							la pala y el centro de la esfera
			pdmc->accion = 1;
		else if(pdmc->esfera1.centro.y<((pdmc->raqueta1.y2+pdmc->raqueta1.y1)/2))
			pdmc->accion = -1;
		else if(pdmc->esfera1.centro.y==((pdmc->raqueta1.y2+pdmc->raqueta1.y1)/2))
			pdmc->accion = 0;
		
		if(pdmc->raqueta2.y1==aux)	//si la pala dos sigue en la misma posicion se decrementa el contador
			contador--;
		else{				//si se mueve la pala se reinicia el contador y se guarda la posicion de la pala
			contador=400;
			aux=pdmc->raqueta2.y1;
		}
		
		if(contador==0)			//han pasado 10 segundos sin moverse la pala2, el bot toma su control
			pdmc->control_r2=true;

		if(pdmc->control_r2){
			if(pdmc->esfera2.centro.y>((pdmc->raqueta2.y2+pdmc->raqueta2.y1)/2))	//distancia en eje y entre el punto medio de la pala y el centro de la esfera
				pdmc->accion2 = 1;
			else if(pdmc->esfera2.centro.y<((pdmc->raqueta2.y2+pdmc->raqueta2.y1)/2))
				pdmc->accion2 = -1;
			else if(pdmc->esfera2.centro.y==((pdmc->raqueta2.y2+pdmc->raqueta2.y1)/2))
				pdmc->accion2 = 0;
		}	
		usleep(25000);
	}
}//((pdmc->raqueta1.y2+pdmc->raqueta1.y1)/2)//pdmc->raqueta1.y2
