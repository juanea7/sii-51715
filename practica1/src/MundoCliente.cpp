// Mundo.cpp: implementation of the CMundo class.
// JUAN ENCINAS ANCHUSTEGUI 51715
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <unistd.h>	//close
#include <fcntl.h>	//open

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	fifo = "/tmp/fifo";		//fifo que va al loger
	
	Init();
}

CMundo::~CMundo()
{
	if(close(t) != 0 || unlink(fifo) !=0){
		perror("error closing fifo a logger(client), se cierra client");
		exit(1);
	}
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);	//jugador 1 color rojo
	print(cad,10,0,1,0,0);
	sprintf(cad,"Jugador2: %d",puntos2);	//jugador 2 color azul
	print(cad,650,0,0,0,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();

	for(int i=0;i<esferas.size();i++)
	{
		esferas[i]->Dibuja();
	}

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	
		
	if(tiempo--<0)		//cada 30 segundos se añade una esfera nueva
	{
		tiempo=1200;
		esferas.push_back(new Esfera);	
	}
	

	for(int i=0;i<esferas.size();i++)		//modificacion del codigo para utilizar el vector de esferas
	{
		esferas[i]->Mueve(0.025f);
		jugador1.Rebota(*esferas[i]);
		jugador2.Rebota(*esferas[i]);
		if(fondo_izq.Rebota(*esferas[i]))
		{	
			for(int j=0;j<esferas.size();j++)	//elimino todas las esferas
				delete esferas[j];
			esferas.clear();
			Esfera * e= new Esfera;
			e->centro.x=0;

			e->radio=0.5f;	//devuelvo el radio a su valor inicial

			e->centro.y=rand()/(float)RAND_MAX;
			e->velocidad.x=2+2*rand()/(float)RAND_MAX;
			e->velocidad.y=2+2*rand()/(float)RAND_MAX;
			esferas.push_back(e);
			puntos2++;
			char cad1[56];
			sprintf(cad1,"Jugador 2 marca 1 punto, lleva un total de %d puntos.\n",puntos2);
			
			int ffml_error = write(t,cad1,sizeof(cad1)); //se guarda el valor de retorno por si hubiese error			
			if(ffml_error==-1){
				perror("error escritura logger(client), se cierra client");
				this->~CMundo();		
				//exit(1);
			}
			else if(ffml_error==0){
				perror("fifo cerrado from logger(client), se cierra client");
				this->~CMundo();		
				//exit(1);
			}
			tiempo=1200;	//vuelve a empezar el temporizador de adicion de esfera
		}

		if(fondo_dcho.Rebota(*esferas[i]))
		{
			for(int j=0;j<esferas.size();j++)
				delete esferas[j];
			esferas.clear();
			Esfera * e= new Esfera;
			e->centro.x=0;

			e->radio=0.5f;	//devuelvo el radio a su valor inicial

			e->centro.y=rand()/(float)RAND_MAX;
			e->velocidad.x=-2-2*rand()/(float)RAND_MAX;
			e->velocidad.y=-2-2*rand()/(float)RAND_MAX;
			esferas.push_back(e);
			puntos1++;
			char cad2[56];
			sprintf(cad2,"Jugador 1 marca 1 punto, lleva un total de %d puntos.\n",puntos1);
			
			int ffml_error_2 = write(t,cad2,sizeof(cad2)); //se guarda el valor de retorno por si hubiese error			
			if(ffml_error_2==-1){
				perror("error escritura logger(client), se cierra client");
				this->~CMundo();		
				//exit(1);
			}
			else if(ffml_error_2==0){
				perror("fifo cerrado from logger(client), se cierra client");
				this->~CMundo();		
				//exit(1);
			}
			tiempo=1200;	//vuelve a empezar el temporizador de adicion de esfera
		}
	}

	int i;
	for(i=0;i<paredes.size();i++)
	{
		for(int j=0;j<esferas.size();j++)
		{
			paredes[i].Rebota(*esferas[j]);
		}
		
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}
	


	//datos pasador por socket al servidor
	char cad_ffms[200];
	
	if(sc_comunicacion.Receive(cad_ffms,sizeof(cad_ffms))<0)
	{
		perror("Error al recibir coordenadas desde servidor (cliente)");
		this->~CMundo();
	}
	
	sscanf(cad_ffms,"%f %f %f %f %f %f %f %f %f %f %d %d", &esferas[0]->centro.x,&esferas[0]->centro.y, &jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2, &jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2, &puntos1, &puntos2);
	
}

void CMundo::Keyboard() 	//control del teclado
{

	if(sc_comunicacion.Send((char *)keystates,sizeof(keystates))<0)
	{
		perror("error escritura client to server(client), se cierra client");
		this->~CMundo();
	}
	
}



void CMundo::Init()
{
	//socket cliente servidor
	char ip[] = "127.0.0.1";
	int port = 4200;
	char nombre[]="socket cliente 1";
	if(sc_comunicacion.Connect(ip,port)<0)
	{
		perror("error al conectar con el servidor socket(cliente)");
		this->~CMundo();		
	}
	if(sc_comunicacion.Send(nombre,strlen(nombre)+1)<0)
	{
		perror("error al enviar el nombre del socket al servidor(cliente)");
		this->~CMundo();
	}
	//fifo to loger	
	t = open(fifo,O_WRONLY);
	if(t==-1){
		perror("errpr open fifo to logger(client), se cierra client");
		this->~CMundo();	
	}	

	tiempo=1200;	//inicio tiempo con 1200 que entre 25 ms da 30 segundos	
	esferas.push_back(new Esfera);	//creo esfera y la añado al vector

	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;
	fondo_izq.setColor(0,1,0);		//la porteria izquierda sera verde

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;
	fondo_dcho.setColor(0,1,0);		//la porteria derecha sera verde

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;
	jugador1.setColor(1,0,0);		//jugador 1 tendra color rojo

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
	jugador2.setColor(0,0,1);		//jugador 2 tendra color rojo
	
	
		

}
