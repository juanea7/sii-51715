// Esfera.cpp: implementation of the Esfera class.
// JUAN ENCINAS ANCHUSTEGUI 51715
//////////////////////////////////////////////////////////////////////

#include "Esfera.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()
{
	radio=0.5f;
	velocidad.x=3;
	velocidad.y=3;
}

Esfera::~Esfera()
{

}



void Esfera::Dibuja()
{
	glColor3ub(255,255,0);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Esfera::Mueve(float t)
{
	centro.x+=velocidad.x*t;
	centro.y+=velocidad.y*t;

	if(radio > 0.25f)	
		radio-=0.0003f;		//el radio disminuye hasta ser de 0.1
	if(velocidad.x < 5.0f)
		velocidad.x+=0.001f * velocidad.x;	//en ese momento empieza a aumentar la velocidad
	else
		velocidad.x=5.0f;
	if(velocidad.y < 5.0f)
		velocidad.y+=0.001f * velocidad.y;
	else
		velocidad.y=5.0f;
}
