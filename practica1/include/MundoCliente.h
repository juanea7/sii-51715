// Mundo.h: interface for the CMundo class.
// JUAN ENCINAS ANCHUSTEGUI 51715
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"
#include "Raqueta.h"

#include "DatosMemCompartida.h"

#include "Socket.h"

class CMundo  
{
public:
	//const char* fifo_from_server;	//identificador de la tuberia del servidor al cliente 
	//int fd_ffms;			//descriptor fifo del servidor al cliente

	
	const char* fifo;	//identificador de la tuberia al logger
	int t;			//descriptor fifo al logger

	//const char* fifo_from_client; 	//identidicador de la tuverua del client a server (teclas)
	//int fd_ffmc;			//descriptor fifo del client al serverd


	Socket sc_comunicacion; //socket comunicacion servidor

	void Init();
	CMundo();
	virtual ~CMundo();	
	
	void InitGL();	
	//void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	

	void Keyboard();		//control del teclado

	//Esfera esfera;

	std::vector<Esfera *> esferas;	//contenedor de esferas

	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	int puntos1;
	int puntos2;

	bool keystates[256];	//vector que guarda las posiciones de las teclas(1_pulasda;0_sin pulsar)

	int tiempo;		//contador de tiempo para añadir esfera

};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
