// Raqueta.h: interface for the Raqueta class.
// JUAN ENCINAS ANCHUSTEGUI 51715
//////////////////////////////////////////////////////////////////////
#ifndef RAQUETA_INC
#define RAQUETA_INC

#include "Plano.h"
#include "Vector2D.h"

class Raqueta : public Plano  
{
public:
	Vector2D velocidad;

	Raqueta();
	virtual ~Raqueta();

	void Mueve(float t);
};


#endif
